import React,  {Component} from "react"

import 'bootstrap/dist/css/bootstrap.min.css';

class MyCustomFooter extends Component{

    constructor(props){
        super(props)
    }

    render(){
        return(
            <footer className="mastfoot mt-auto">
                <div className="inner">
                    {this.props.name}
                </div>
            </footer>
        )
    }
}

export default MyCustomFooter