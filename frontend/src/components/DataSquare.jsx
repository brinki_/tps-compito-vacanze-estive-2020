import React, {Component} from "react"
import { Container, Row, Col } from "react-bootstrap"

import "../styles/data-square.css"

class DataSquare extends Component{

    constructor(props){
        super(props)

    }

    render(){
        return(
            <div className="square-data">
                <Container>
                    <Col>
                        <Row className="justify-content-center">
                            <h4><strong>{this.props.sensorIndex}</strong></h4>
                        </Row>
                        <br/>
                    </Col>
                </Container>
            </div>
        );
    }
}

export default DataSquare;