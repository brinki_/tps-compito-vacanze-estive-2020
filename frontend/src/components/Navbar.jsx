import React, {Component} from "react"
import {Nav, Navbar, NavDropdown, Form, FormControl, Button} from "react-bootstrap"
class MyCustomNavbar extends Component{
    render(){
        return(
            <Navbar bg="dark" expand="lg">
                <Navbar.Brand href="/" style={{color:"#ffffff"}}><strong>Gestore Vasca di Rifiuti</strong></Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto" >
                    </Nav>
                    <Nav>
                        <Nav.Link href="/login">
                            <Button variant="outline-light">
                                Login
                            </Button>
                        </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default MyCustomNavbar