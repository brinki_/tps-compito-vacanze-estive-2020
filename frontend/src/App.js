import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import React, {Component} from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom"

import MyCustomNavbar from "./components/Navbar"

import Homepage from "./routes/Homepage"
import Login from "./routes/Login"
import Management from './routes/Management';


class App extends Component{
  render(){
    return (
      <div>
        <MyCustomNavbar float="top"></MyCustomNavbar>
        <div className="main-page-content">
            <BrowserRouter >
              <Switch>
                <Route exact path="/" component={Homepage}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/management" component={Management}/>
              </Switch>
            </BrowserRouter>
        </div>
      </div>
    )
  }
}

export default App;
