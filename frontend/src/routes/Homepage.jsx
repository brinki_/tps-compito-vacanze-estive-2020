import React , {Component, Link} from "react"
import { Container, Row, Col, Table } from "react-bootstrap"
import DataSquare from "../components/DataSquare"



import "../styles/homepage.css"
import Charts from "./Charts"

class Homepage extends Component{

    render(){
        return(
            <div className="homepage">
               <Container >
                   <Row>
                        <Col>
                            <Row className="justify-content-center">                                
                                <h4 float="top"><strong>Vista sistema</strong></h4>                                
                            </Row>
                        </Col>
                        <Col>
                            <Row className="justify-content-center">
                                <h4 float="top"><strong>Andamento Temperatura</strong></h4>
                                <br/><br/><br/>
                                <Charts/>
                            </Row>
                        </Col>
                        <Col>
                            <Row className="justify-content-center">                                
                                <h4 float="top"><strong>Overview Dati</strong></h4>                     
                            </Row>
                            <Row>

                            </Row>
                        </Col>
                   </Row>
               </Container>
            </div>
        );
    }
}

export default Homepage;