import React , {Component} from "react"
import '../styles/charts.css'
import { LineChart, Line, CartesianGrid, XAxis, YAxis } from 'recharts';
import { Row, Form, Dropdown, DropdownButton, NavDropdown, Tooltip } from "react-bootstrap";

class Charts extends Component{
    constructor(props){
        super(props)
        this.state = {
            loadingSensors: true,
            loadingData: false,
            sensors: "",
            selectedSensor: 1,
            sensorData: ""
        }
    }

    async componentDidMount(){
        const axios = require('axios').default
        const response = await axios.get('http://localhost:5000/getSensors')

        this.setState({
            loadingSensors:false,
            sensors: response.data.sensors
        })
    }

    handleSensorSelected = async (key, e) => {
        this.setState({selectedSensor: key})
        const axios = require('axios').default
        const response = await axios.get(
            'http://localhost:5000/getDataPerSensor?sensor=' + key
        )
        console.log(response.data)
        this.setState({
            sensorData : response.data,
            loadingSensors: false
        })

    }


    render(){
        return(
            <div className="charts">
                <Row>
                    {
                        this.state.sensorData !== "" ?
                            <LineChart width={500} height={300} data={this.state.sensorData.sensors}>
                                <Line type="monotone" dataKey="value" stroke="#8884d8" />
                                <CartesianGrid stroke="#ccc" strokeDasharray="5 5"/>
                                <XAxis dataKey="time"/>
                                <YAxis/>
                                <Tooltip/>
                            </LineChart>
                        :
                        <p>Loading...</p>
                    }
                </Row>
                <Form>
                    <Row className="justify-content-center">
                        <Form.Label>
                            <strong>Sensore:</strong>
                        </Form.Label>

                        <Dropdown id="dropdown-basic-button" title="Dropdown button" style={{left:"4px"}} onSelect={this.handleSensorSelected}>
                            <Dropdown.Toggle className="font-dropdown">
                                {this.state.selectedSensor}
                            </Dropdown.Toggle>
                            <Dropdown.Menu>
                                {
                                    this.state.loadingSensors ? <p>Loading...</p> :
                                    this.state.sensors.map((sensore, i) => 
                                        <Dropdown.Item eventKey={sensore.index}>{sensore.index}</Dropdown.Item>
                                    )
                                }
                            </Dropdown.Menu>
                        </Dropdown>
                    </Row>
                </Form>

               
            </div>
        );
    }
}

export default Charts;