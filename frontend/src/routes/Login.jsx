import React, {Component} from "react"
import 'bootstrap/dist/css/bootstrap.min.css';
import { Form, Button, Row, Col } from "react-bootstrap";
import img from "../images/favicon-temperatura.png"
class Login extends Component{

    constructor(props){
        super(props)
        this.state = {
            username : "",
            password: "",
            logged: false,
            loading: false
        }
        this.handleLogin = this.handleLogin.bind(this)
    }

    async handleLogin(){
        console.table(this.state.username, this.state.password)
        if(this.state.loading === false){
            const axios = require('axios').default
            const response = await axios.post(
                'http://localhost:5000/login', 
                {
                    'username' : this.state.username,
                    'password' : this.state.password
                }, 
                {
                    headers: {'Content-Type': 'application/json'}
                }
            )

            if(response.data.status === "successful"){
                console.log("ok")
                window.location.pathname="/management"
            } else{
                alert("Username e/o password errati")
            }

            this.setState({
                loading: false
            })
        }
    
    }

    render(){
        return(
            <div>
                <Row className="justify-content-center" md={4}>
                    <Form >
                        <Col>
                            <Row className="justify-content-center">
                                <img className="mb-4" src={img} alt="" width="72" height="72" float="top"/>
                            </Row>
                            <h1 className="h3 mb-3 font-weight-normal" style={{textAlign:"center"}}><strong>Login</strong></h1>
                            <Row>
                                <Form.Label>Nome Utente</Form.Label>
                                <Form.Control type="input" placeholder="Nome Utente" onChange={(e) => {this.setState({username : e.target.value})}}/>
                            </Row>
                            <br/>
                            <Row>
                                <Form.Label>Password</Form.Label>
                                <Form.Control type="password" placeholder="Password" onChange={(e) => {this.setState({password : e.target.value})}}/>
                            </Row>
                            <br/>
                            <Row className="justify-content-center">
                                <Button className="btn btn-lg btn-primary btn-block" onClick={this.handleLogin}><strong>Log in</strong></Button>
                                <p className="mt-5 mb-3 text-muted">&copy; Luca Brini - A.S. 2020-2021</p>
                            </Row>
                        </Col>
                    </Form>
                </Row>
            </div>
        )
    }
}



export default Login