import mysql.connector

class DatabaseManager:

    def __init__(self, host, user, password, database):
       self.__db = mysql.connector.connect(
           host=host,
           user=user,
           password=password,
           db=database
           
       )
       self.__cursor = self.__db.cursor(dictionary=True)

    def getAreasNumber(self):
        self.__cursor.execute("""
            SELECT * FROM `areas`
        """)

        return(len(self.__cursor.fetchall()))

    def getSensorsNumber(self):
        self.__cursor.execute("""
            SELECT * FROM `sensors`
        """)

        return(len(self.__cursor.fetchall()))

    def addDataRow(self, value, sensor):
        print("sensor:", sensor)
        print("value: ", value)
        self.__cursor.execute("""
            INSERT INTO `data` (
                `sensor`,
                `value`
            ) VALUES (
                %d,
                %d
            )
        """ % (sensor, value))
        self.__db.commit()

    def getMaxTemp(self):
        self.__cursor.execute("""
            SELECT MAX(`index`)
            FROM `max_temp`
            WHERE `index` >= 0
        """)

        lastRowIndex = self.__cursor.fetchall()[0]
        print(lastRowIndex)

        self.__cursor.execute("""
            SELECT `value` FROM `max_temp`
            WHERE `index` = %d
        """ % lastRowIndex)

        return self.__cursor.fetchall()[0]['value']