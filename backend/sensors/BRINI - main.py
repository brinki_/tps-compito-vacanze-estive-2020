from BRINI_DatabaseManager import DatabaseManager
from BRINI_SensorsManager import SensorManager
from BRINI_FansManager import FansManager
import time

databaseManager = DatabaseManager('localhost', 'root', 'roottoor', 'tpsvacanzestive')
sensorManager = SensorManager()
fansManager = FansManager()


sensorsNumber = databaseManager.getSensorsNumber()
areasNumber = databaseManager.getAreasNumber()
maxTemp = 30
delayInSeconds = 60


while(True):
    for i in range(1, sensorsNumber+1, 1):
        value = sensorManager.readValue(i, 60)
        print(value)
        databaseManager.addDataRow(value, i)

        if(value > maxTemp):
            if(i >= 1 and i <= 4):
                fansManager.activateFan(1)
            elif(i>=5 and i<=8):
                fansManager.activateFan(2)
            elif(i>=9 and i<=12):
                fansManager.activateFan(3)
            elif(i>=13 and i<=16):
                fansManager.activateFan(4)
    time.sleep(delayInSeconds)