USE sql2362989;

CREATE TABLE `data` (
	`index` INT PRIMARY KEY AUTO_INCREMENT,
	`time` DATETIME DEFAULT NOW(),
    `value` INT NOT NULL,
    `sensor` INT NOT NULL,
    FOREIGN KEY (`sensor`) REFERENCES `sensors`(`index`)
);

CREATE TABLE `users` (
	`index` INT PRIMARY KEY AUTO_INCREMENT,
	`username` TEXT UNIQUE,
    `password` TEXT NOT NULL,
    `codice_fiscale` VARCHAR(16),
    `type` INT DEFAULT 0,
    `creation_date` DATETIME DEFAULT NOW()
);

CREATE TABLE `areas` (
	`index` INT PRIMARY KEY AUTO_INCREMENT,
	`name` TEXT NOT NULL UNIQUE,
    `creation_date` DATETIME DEFAULT NOW()
);

CREATE TABLE `sensors` (
	`index` INT PRIMARY KEY,
    `area` INT NOT NULL,
    FOREIGN KEY (`area`) REFERENCES `areas`(`index`)
);

CREATE TABLE `max_temp` (
    `index` INT PRIMARY KEY AUTO_INCREMENT,
    `value` INT NOT NULL
)

CREATE TABLE `medium_temp` (
    `index`INT PRIMARY KEY AUTO_INCREMENT,
    `sensor` INT NOT NULL,
    `value` INT NOT NULL,
    `time` DATE DEFAULT NOW()
    FOREIGN KEY(`sensor`) REFERENCES `sensors`(`index`)
)

COMMIT;