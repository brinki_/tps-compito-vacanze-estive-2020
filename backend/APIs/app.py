#-------------------------------------------------------------------------------------------
from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin
from flask_mysqldb import MySQL, MySQLdb
import json
import os
import datetime

#-------------------------------------------------------------------------------------------

app = Flask(__name__)
CORS(app)

#-------------------------------------------------------------------------------------------
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['MYSQL_USER'] = 'root' #'creappa_admin'
app.config['MYSQL_PASSWORD'] = 'roottoor'
app.config['MYSQL_HOST'] = 'localhost'
app.config['MYSQL_DB'] = 'tpsvacanzestive'
app.config['MYSQL_CURSORCLASS'] = 'DictCursor'

mysql = MySQL(app)

#-------------------------------------------------------------------------------------------
#login method

def dbLogin(cursor, email, password):

    cursor.execute("""
        SELECT `password` FROM `users` WHERE (`username` = '%s')
    """ % email) 

    loginQueryResults = cursor.fetchall()

    if(len(loginQueryResults) == 0):
        return {"status" : "badUsername"}
    else:
        if(loginQueryResults[0]['password'] == password):
            return {"status": "successful"}
        else:
            return {"status" : "badPassword"}

#-------------------------------------------------------------------------------------------
#login
@app.route('/login', methods=['POST'])
def login():
    cursor = mysql.connection.cursor()

    try:
        username = request.json['username']
        password = request.json['password']
    except (TypeError, KeyError):
        return jsonify({'status' : 'invalidData'})

    print(username, password)

    if(len(password) < 0 ):
        return jsonify({'status' : 'invalidPassword'})
    
    if(len(username) <= 0):
        return jsonify({'status' : 'invalidUsername'})

    return jsonify(dbLogin(cursor, username, password))

#-------------------------------------------------------------------------------------------
#register user

@app.route('/registerUser', methods=['POST'])
def registerUser():
    cursor = mysql.connection.cursor()

    try:

        email = request.json['email']
        password = request.json['password']
        codiceFiscale = request.json['codiceFiscale']

    except (TypeError, KeyError):
        return jsonify({'status' : 'invalidData'}) 
    
    if(len(password) < 8 ):
        return jsonify({'status' : 'invalidPassword'})
    
    if(len(email) <= 0 or ('@' not in email) or ('.' not in email)):
        return jsonify({'status' : 'invalidEmail'})

    if(len(codiceFiscale) < 16):
        return jsonify({'status' : 'invalidCodiceFiscale'})

    try:
        cursor.execute("""
            INSERT INTO users(
                email,
                `password`,
                codice_fiscale,
            ) VALUES (
                '%s',
                '%s',
                '%s'

            );
        """ % (email, password, str(codiceFiscale).upper))
        
        mysql.connection.commit()
        return jsonify({"status" : "successful"})

    except MySQLdb.IntegrityError:

        cursor.execute("SELECT email FROM users WHERE email = '%s'" % email)
        results = cursor.fetchall()

        if(len(results)):
            return jsonify({"status" : "alreayExistingEmail"})
        else:
            return jsonify({"status" : "generalDBError"})

@app.route('/getAllData', methods=['GET'])
def getData():
    cursor = mysql.connection.cursor()

    cursor.execute("""
        SELECT `value`, `time`, `sensor` FROM `data`
    """)

    rawData = cursor.fetchall()
    data = []

    for rD in rawData:
        data.append({
            'datetime' : rD['time'],
            'value' : rD['value'],
            'sensor' : rD['sensor']
        })

    return jsonify({
        'status' : 'successful',
        'data' : data
    })

@app.route('/getSensors', methods=['GET'])
def getSensors():
    try:
        cursor = mysql.connection.cursor()
        cursor.execute("""
            SELECT * FROM `sensors`
        """)
    except MySQLdb.Error as err:
        return jsonify({
            'status' : 'generalDatabaseError',
            'err' : str(err)
        })

    rawSensors = cursor.fetchall()

    sensors = []
    for s in rawSensors:
        sensors.append({
            'index' : s['index'],
            'area' : s['area']
        })

    return jsonify({'sensors' : sensors})


@app.route('/addSensor', methods=['POST'])
def addSensor():
    cursor = mysql.connection.cursor()

    try:
        email = request.json['email']
        password = request.json['password'],
        area = request.json['areaIndex']
    except (TypeError, KeyError):
        return jsonify({'status' : 'invalidData'})
    
    if(len(password) < 8 ):
        return jsonify({'status' : 'invalidPassword'})
    
    if(len(email) <= 0 or ('@' not in email) or ('.' not in email)):
        return jsonify({'status' : 'invalidEmail'})

    try:
        int(area)
    except ValueError:
        return jsonify({'status' : 'invalidAreaIndex'})

    if(dbLogin(cursor, email, password) != 'successful'):
        return jsonify({'status' : 'notAuthorized'})

    try:
        cursor.execute("""
            INSERT INTO `sensors` (
                `area`
            ) VALUES (
                %d
            )
        """ % area)
    except MySQLdb.Error as err:
        return jsonify({
            'status' : 'generalDatabaseError',
            'error' : str(err)
        })

    return jsonify({'status' : 'successful'})

    
@app.route('/addArea', methods=['POST'])
def addArea():
    cursor = mysql.connection.cursor()

    try:
        email = request.json['email']
        password = request.json['password'],
        areaName = request.json['areaName']
    except (TypeError, KeyError):
        return jsonify({'status' : 'invalidData'})
    
    if(len(password) < 8 ):
        return jsonify({'status' : 'invalidPassword'})
    
    if(len(email) <= 0 or ('@' not in email) or ('.' not in email)):
        return jsonify({'status' : 'invalidEmail'})

    if(len(areaName) < 0):
        return jsonify({'status' : 'invalidAreaName'})

    if(dbLogin(cursor, email, password) != 'successful'):
        return jsonify({'status' : 'notAuthorized'})

    try:
        cursor.execute("""
            INSERT INTO `areas` (
                `name`
            ) VALUES (
                '%s'
            )
        """ % areaName)
    except MySQLdb.Error as err:
        return jsonify({
            'status' : 'generalDatabaseError',
            'error' : str(err)
        })

    return jsonify({'status' : 'successful'})

@app.route('/getDataPerSensor', methods=['GET'])
def getDataPerSensor():

    try:
        sensor = request.args.get('sensor')
    except ValueError:
        return jsonify({'status' : 'missingSensor'})

    try:
        cursor = mysql.connection.cursor()
        cursor.execute("""
            SELECT * FROM `data` WHERE `sensor` = %d
        """ % int(sensor))
    except MySQLdb.Error as err:
        return jsonify({
            'status' : 'generalDatabaseError',
            'err' : str(err)
        })

    rawData = cursor.fetchall()

    data = []
    for d in rawData:
        data.append({
            'index' : d['index'],
            'value' : d['value'],
            'time' : d['time']
        })

    return jsonify({'sensors' : data})


    


if __name__ == "__main__":
    app.run(debug=True) 