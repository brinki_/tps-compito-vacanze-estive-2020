use tpsvacanzestive;

INSERT INTO `areas` (
	`name`
) VALUES ('a1'), ('a2'), ('b1'), ('b2');

INSERT INTO `sensors` (
	`area`
) VALUES (1),(1),(1),(1),(2),(2),(2),(2),(3),(3),(3),(3),(4),(4),(4),(4);

INSERT INTO `users` (
	`username`,
    `password`,
    `codice_fiscale`
) VALUES (
	'admin',
    'admin',
    '0000000000000000'
);

commit;